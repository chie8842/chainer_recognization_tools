import os
import sys
import math
import shutil

srcparentdir = os.path.abspath(sys.argv[1])
targetdir = os.path.abspath(sys.argv[2])

trdir = targetdir + "/" + "trdata"
evdir = targetdir + "/" + "evdata"

for targetdir in [targetdir, trdir, evdir]:
  if not os.path.isdir(targetdir):
    os.mkdir(targetdir)

# ソースディレクトリごとに処理 
for i,srcdir in enumerate(os.listdir(srcparentdir)):
  print(srcdir + "," + str(i))
  f = open("labellist.txt", "a")
  f.write(srcdir + "," + str(i) + "\n")
  f.close()
  srcdir_abs = srcparentdir + "/" + srcdir
  if os.path.isdir(srcdir_abs):
    itemlist = os.listdir(srcdir_abs)
#全体の85%をtrainingdata、残りをevaluatedataとする。
    filenum = len(itemlist)
    trnum = round(filenum * 0.85, 0)
    evnum = filenum - trnum

    print("全データ数：" + str(filenum) + ", 学習データ数:" + str(trnum) + ", 評価データ数:" + str(evnum))
    for j,item in enumerate(itemlist):
#     root,ext = os.path.splitext(item)
#     print(root + ",," + ext)
#     if ext in ['jpg', 'jpeg', 'png']:
       # print(src + "," + str(j))
      if j < trnum:
        print(srcdir_abs + "/" + item, trdir + "/" + item)
        shutil.copyfile(srcdir_abs + "/" + item, trdir + "/" + item)
        f = open("training.txt", "a")
        f.write(trdir + "/" + item + " " + str(i) + "\n")
        f.close()
      else:
        print(srcdir_abs + "/" + item, evdir + "/" + item)
        shutil.copyfile(srcdir_abs + "/" + item, evdir + "/" + item)
        f = open("test.txt", "a")
        f.write(evdir + "/" + item + " " + str(i) + "\n")
        f.close()
        
