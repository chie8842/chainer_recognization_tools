# -*- coding:utf-8 -*-

import os
import urllib.request
import bs4
for sex in ["mens", "ladys"]:

# ディレクトリがなければ作成する
  dirName = "orgdata/" + sex
  if not os.path.isdir(dirName):
    os.mkdir(dirName)

# HTML を取得
  org_htmlstr = "http://beauty.hotpepper.jp/CSP/bt/hairCatalogSearch/" + sex + "/condtion/?pn="
  cnt = 0 
  for pgnum in range(100, 500):
    htmlstr = org_htmlstr + str(pgnum)
    print(htmlstr)
    html = urllib.request.urlopen(htmlstr).read()

    soup = bs4.BeautifulSoup(html, 'html.parser')
    if cnt > 2000:
      break
    else:
      main = soup.find("ul", attrs={"class": "cFix rankingList"})

      for link in main.findAll("a", attrs={"class": "db pr"}):
        cnt += 1
        linkhtmlstr = link.get("href")
        linkhtml = urllib.request.urlopen(linkhtmlstr).read()

        soup2 = bs4.BeautifulSoup(linkhtml, 'html.parser')
        img = soup2.find("img", attrs={"name": "main"})
        imgsrc = img.get("src")
        fname = imgsrc.split("/")[-1]
        # 画像を保存
        if not os.path.exists(dirName + "/" + fname):
           urllib.request.urlretrieve(imgsrc, dirName + "/" + fname)
#	   request = urllib.request.urlopen(linkhtmlstr)
#           f = fopen(dirName + "/" + fname, "wb")
#           f.write(request.read())
#           f.close()
           print(imgsrc + "," + fname + ", cnd=" + str(cnt))
      print("next page")
